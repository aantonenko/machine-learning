from hierarchic_single_link_clustering import HierarchicClustering

data = [(0, 0), (1, 0), (5, 4), (0, 1), (5, 1), (2, 2), (1, 1)]

alg = HierarchicClustering(data)
alg.clustering()
for clusters in alg.dendrogram:
    print clusters
alg.plot_dendrogram()