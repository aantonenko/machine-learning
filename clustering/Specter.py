import math
from abc import abstractmethod

import matplotlib.pyplot as plt
from scipy.spatial.distance import pdist, squareform

from util.utils import copy_list


class Specter(object):
    def __init__(self, data):
        self.data = data
        self.specter = []
        self.metric_values = []

        self._not_in_specter = []
        copy_list(self.data, self._not_in_specter)

        distances = pdist(self.data, metric='euclidean')
        self._similarity_matrix = math.ceil(max(distances)) - squareform(distances)  # build similarity matrix
        for i in xrange(len(self._similarity_matrix)):
            self._similarity_matrix[i, i] = 0

    def clustering(self):
        self.specter = [self._not_in_specter[0]]
        self._not_in_specter.pop(0)
        while len(self.data) != len(self.specter):
            new_sample, value = self._nearest_sample()
            self._not_in_specter.pop(self._not_in_specter.index(new_sample))
            self.specter.append(new_sample)
            self.metric_values.append(value)

    def _nearest_sample(self):
        finder_sample = []
        criteria = self._get_start_metric_value()
        for sample in self._not_in_specter:
            metric = self._metric_for_specter_and_sample(sample)
            if self._apply_criteria(criteria, metric):
                criteria = metric
                finder_sample = sample
        return finder_sample, criteria

    def _metric_for_specter_and_sample(self, sample):
        metric_sum = 0
        for item in self.specter:
            metric_sum += self.metric(item, sample)
        return metric_sum / len(self.specter)

    @abstractmethod
    def _get_start_metric_value(self):
        pass

    @abstractmethod
    def _apply_criteria(self, criteria, metric):
        pass

    @abstractmethod
    def metric(self, a, b):
        pass

    def plot(self):
        plt.scatter(xrange(1, len(self.metric_values) + 1, 1), self.metric_values)
        for label, x, y in zip(['{0}'.format(point) for point in self.specter[1:]],
                               xrange(1, len(self.metric_values) + 1, 1), self.metric_values):
            plt.annotate(label, xy=(x, y), xytext=(-20, 20),
                         textcoords='offset points', ha='right', va='bottom',
                         bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
                         arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))
        plt.annotate('start with {0}'.format(self.specter[0]), xy=(0, 0), xytext=(10, 10), textcoords='offset points')
        plt.show()


class SpecterBySimilarity(Specter):
    def _get_start_metric_value(self):
        return 0

    def _apply_criteria(self, criteria, metric):
        return metric > criteria

    def metric(self, a, b):
        return self._similarity(a, b)

    def _similarity(self, a, b):
        index_a = self.data.index(a)
        index_b = self.data.index(b)
        return self._similarity_matrix[index_a, index_b]


class SpecterByDistance(Specter):
    def _get_start_metric_value(self):
        return float("inf")

    def _apply_criteria(self, criteria, metric):
        return metric < criteria

    def metric(self, a, b):
        return self.euclid_distance(a, b)

    @staticmethod
    def euclid_distance(a, b):
        x1, y1 = a
        x2, y2 = b
        return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
