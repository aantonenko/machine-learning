from random import randint
from specter import SpecterByDistance
from util.utils import copy_list, plot_clusters


class FOREL():
    """
    data clustering Algorithm FORmal ELement
    """
    def __init__(self, data, radius):
        self.data = data
        self._not_clustering = []
        copy_list(data, self._not_clustering)
        self.radius = radius
        self.clusters = []

    def clustering(self):
        while len(self._not_clustering) > 0:
            current_object = self._not_clustering[randint(0, len(self._not_clustering) - 1)]
            neighbour_objects = self._get_neighbour_objects(current_object)
            center_object = self._center_of_objects(neighbour_objects)

            while center_object != current_object:
                current_object = center_object
                neighbour_objects = self._get_neighbour_objects(current_object)
                center_object = self._center_of_objects(neighbour_objects)
            self._delete(neighbour_objects)
            self.clusters.append(neighbour_objects)

    def _get_neighbour_objects(self, center_object):
        neighbours = []
        for o in self._not_clustering:
            if SpecterByDistance.euclid_distance(center_object, o) < self.radius:
                neighbours.append(o)
        return neighbours

    @staticmethod
    def _center_of_objects(neighbour_objects):
        x, y = 0, 0
        for o in neighbour_objects:
            x += o[0]
            y += o[1]
        return x/len(neighbour_objects), y/len(neighbour_objects)

    def _delete(self, neighbour_objects):
        for o in neighbour_objects:
            self._not_clustering.remove(o)

    def plot(self):
        plot_clusters(self.clusters)