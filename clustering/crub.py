from scipy.spatial.distance import pdist, squareform
from util.queue import PriorityQueue
from itertools import count
import matplotlib.pyplot as plt
from util.utils import plot_clusters


class Crub(object):
    colors = ['r', 'g', 'b', 'y', 'o']

    def __init__(self, data):
        self.data = data
        self.matrix = squareform(pdist(self.data, metric='euclidean'))  # distance matrix
        for i in xrange(len(self.matrix)):
            self.matrix[i, i] = 0
        self._mst = []
        self.clusters = []

    def build_min_spanning_tree(self):  # Prim algorithm
        self._mst = self.mst_prim()

    def clustering(self, cluster_count):
        edges = self.find_largest_edges(cluster_count - 1)
        print edges
        k = 0
        for i in xrange(len(self._mst) - 1):
            if self._is_in_edges([self._mst[i], self._mst[i + 1]], edges):
                self.clusters.append(self._mst[k:i + 1])
                k = i + 1
        self.clusters.append(self._mst[k:])

    def plot_clusters(self):
        plot_clusters(self.clusters)

    @staticmethod
    def _is_in_edges(tested_edge, edges):
        for edge in edges:
            if tested_edge[0] == edge[0] and tested_edge[1] == edge[1]:
                return True
        return False

    def find_largest_edges(self, n):
        edges = []
        for i in xrange(len(self._mst) - 1):
            self._get_distance(self._mst[i], self._mst[i + 1])
            edges.append([self._mst[i], self._mst[i + 1], self._get_distance(self._mst[i], self._mst[i + 1])])
        edges.sort(key=lambda tup: tup[2], reverse=True)
        return edges[0:n]

    def mst_prim(self):
        queue = PriorityQueue()
        for item in self.data:
            queue.add(item)
        queue.add(self.data[0], priority=0)  # choose root of mst

        mst = []
        while queue.size() > 0:
            u = queue.pop()
            mst.append(u)
            for v in queue.get_all():
                if self._get_distance(u, v) < queue.get_priority(v):
                    queue.add(v, self._get_distance(u, v))  # update priority
        return mst

    def plot_mst(self):
        counter = count()
        plt.plot([float(i[0]) for i in self._mst], [float(i[1]) for i in self._mst])
        for label, x, y in zip(['{0} - {1}'.format(next(counter), point) for point in self._mst],
                               [float(i[0]) for i in self._mst], [float(i[1]) for i in self._mst]):
            plt.annotate(label, xy=(x, y), xytext=(-20, 20),
                         textcoords='offset points', ha='right', va='bottom',
                         bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5))
        plt.show()

    def _get_distance(self, a, b):
        return self.matrix[self.data.index(a), self.data.index(b)]
