from util.utils import copy_list
from specter import SpecterByDistance
import matplotlib.pyplot as plt
from util.utils import colors


class HierarchicClustering():
    def __init__(self, data):
        self.data = data
        self.dendrogram = []
        self.clusters = []
        for i in xrange(len(data)):
            self.clusters.append([])
            self.clusters[i].append(data[i])
        first_clusters = []
        copy_list(self.clusters, first_clusters)
        self.dendrogram.append(first_clusters)

    def clustering(self):
        while len(self.clusters) > 1:
            distances = []
            for i in range(0, len(self.clusters)):
                for j in range(i + 1, len(self.clusters)):
                    distances.append((self.clusters[i], self.clusters[j],
                                      self.euclid_cluster_distance(self.clusters[i], self.clusters[j])))

            distances = sorted(distances, key=lambda x: x[2])

            # marge clusters
            self.clusters.pop(self.clusters.index(distances[0][0]))
            self.clusters.pop(self.clusters.index(distances[0][1]))
            new_cluster = []
            for i in xrange(len(distances[0][0])):
                new_cluster.append(distances[0][0][i])
            for i in xrange(len(distances[0][1])):
                new_cluster.append(distances[0][1][i])
            self.clusters.append(new_cluster)

            c = []  # new dendrogram item
            copy_list(self.clusters, c)
            self.dendrogram.append(c)

    @staticmethod
    def euclid_cluster_distance(a, b):
        min_distance = float('inf')
        for i in range(len(a)):
            for j in range(len(b)):
                distance = SpecterByDistance.euclid_distance(a[i], b[j])
                if distance < min_distance:
                    min_distance = distance
        return min_distance

    def plot_dendrogram(self):
        for k in xrange(len(self.dendrogram)):
            plt.subplot(len(self.dendrogram), 1, k + 1)
            for i in xrange(len(self.dendrogram[k])):
                x, y = [], []
                for j in xrange(len(self.dendrogram[k][i])):
                    x.append(self.dendrogram[k][i][j][0])
                    y.append(self.dendrogram[k][i][j][1])
                plt.scatter(x, y, color=colors[i % len(colors)])
        plt.show()