import matplotlib.pyplot as plt


colors = ['r', 'g', 'b', 'y', '#660000', 'c', 'm', 'k', 'orange', 'darkgreen']


def copy_list(a, b):
    for item in a:
        b.append(item)


def plot_clusters(clusters):
    for cluster in clusters:
        plt.scatter([float(i[0]) for i in cluster], [float(i[1]) for i in cluster],
                    c=colors[clusters.index(cluster) % len(colors)])
    plt.show()