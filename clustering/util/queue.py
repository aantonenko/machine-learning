from itertools import count
from heapq import heappop, heappush


class PriorityQueue():
    _REMOVED = '<removed>'

    def __init__(self):
        self._pq = []
        self._entry_finder = {}
        self.counter = count()

    def add(self, item, priority=float('inf')):
        if item in self._entry_finder:
            self.remove(item)
        index = next(self.counter)
        entry = [priority, index, item]
        self._entry_finder[item] = entry
        heappush(self._pq, entry)

    def remove(self, item):
        entry = self._entry_finder.pop(item)
        entry[-1] = PriorityQueue._REMOVED

    def pop(self):
        while self._pq:
            priority, index, item = heappop(self._pq)
            if item is not PriorityQueue._REMOVED:
                del self._entry_finder[item]
                return item
        raise KeyError('pop from an empty priority queue')

    def size(self):
        return len(self._entry_finder)

    def get_all(self):
        return self._entry_finder.keys()

    def get_priority(self, item):
        return self._entry_finder[item][0]